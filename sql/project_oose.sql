-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2015 at 08:15 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project_oose`
--

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE IF NOT EXISTS `configs` (
  `param_name` varchar(16) NOT NULL,
  `value` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`param_name`, `value`) VALUES
('profit_margin', '2');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
`id` int(11) unsigned NOT NULL,
  `barcode` varchar(128) NOT NULL,
  `name` varchar(64) NOT NULL,
  `price` int(11) unsigned NOT NULL,
  `amount` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=ascii;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `barcode`, `name`, `price`, `amount`) VALUES
(1, 'AWX-BBCUD-CRUD-XXX', 'Some object', 10000, 100),
(2, 'AWX-BBCUD-CRUD-TTT', 'Some object rasa stroberi', 12000, 100),
(3, 'AWX-BBCUD-YXPL-XXX', 'Some object vanilla mini edition', 5000, 100),
(4, 'AWX-BBCUD-YXPL-TTT', 'Some object stroberi mini edition', 8000, 100100),
(5, 'AWX-BBCUD-CRUD-XXX', 'some random object', 100, 1100),
(6, 'BRUNH-AWX-HILDE', 'object insert coba', 2000, 100),
(7, 'ABC-ABC-BCA', 'object insert coba lagi', 2000, 100),
(8, 'GFTHST-HHSYGF-GGTHYR', 'magnum', 500000, 191),
(9, 'arpajuyla', 'wwell', 90, 2000),
(10, 'AWX-UPX-TLES', 'someother', 1000, 90),
(11, 'WTF-ABC-WTF-RTF-PPT', 'WTF-INSIDE', 96, 96),
(12, 'REG-ICIDE', 'Regal Suicide :v', 10, 9),
(13, '897380180748', 'KaskusID', 5600, 70);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `count` int(11) unsigned NOT NULL,
  `total_profit` int(11) unsigned NOT NULL,
  `total_price` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ascii;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `count`, `total_profit`, `total_price`, `created_at`) VALUES
(1, 4, 7, 8500, 9000, '2015-05-31 13:21:51'),
(2, 1, 2, 260, 13260, '2015-06-05 08:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `order_lists`
--

CREATE TABLE IF NOT EXISTS `order_lists` (
`id` int(11) unsigned NOT NULL,
  `order_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `amount` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=ascii;

--
-- Dumping data for table `order_lists`
--

INSERT INTO `order_lists` (`id`, `order_id`, `item_id`, `amount`, `created_at`) VALUES
(3, 1, 1, 0, '2015-05-31 13:21:52'),
(4, 1, 2, 0, '2015-05-31 13:21:52'),
(5, 1, 3, 0, '2015-05-31 13:21:52'),
(6, 1, 4, 0, '2015-05-31 13:21:52'),
(7, 1, 5, 0, '2015-05-31 13:21:52'),
(8, 1, 6, 0, '2015-05-31 13:21:52'),
(9, 1, 7, 0, '2015-05-31 13:21:52'),
(10, 2, 3, 1, '2015-06-05 08:15:16'),
(11, 2, 4, 1, '2015-06-05 08:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `type_id` int(11) unsigned NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=ascii;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type_id`, `user_name`, `password`) VALUES
(1, 3, 'Ragnarok Junction', 'aman123'),
(2, 3, 'World of C', 'aman123'),
(3, 3, 'Pedopocalypse', 'aman123'),
(4, 2, 'Oppoi', 'aman123'),
(5, 2, 'RxR', 'aman123'),
(6, 2, '.A', 'aman123'),
(7, 1, 'Anubis', 'aman123'),
(8, 1, 'Sirius', 'aman123'),
(9, 1, 'Giza', 'aman123'),
(10, 1, 'Osiris', 'amonra');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
`id` int(11) unsigned NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ascii;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `type`) VALUES
(1, 'Supplier'),
(2, 'Manager'),
(3, 'Cashier');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
 ADD PRIMARY KEY (`param_name`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_lists`
--
ALTER TABLE `order_lists`
 ADD PRIMARY KEY (`id`), ADD KEY `item_id` (`item_id`), ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_lists`
--
ALTER TABLE `order_lists`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `order_lists`
--
ALTER TABLE `order_lists`
ADD CONSTRAINT `order_lists_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `order_lists_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `user_types` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
