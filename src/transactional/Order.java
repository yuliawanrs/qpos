/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactional;
import java.util.ArrayList;

/**
 *
 * @author Tezla
 */
public class Order {
    private static int profit_margin;
    private ArrayList<Item> orderList;
    private int count;
    private String date;
    private String time;
    private int totalPrice;
    private int totalProfit;
    
    public Order(){
        orderList = new ArrayList();
        orderList.ensureCapacity(10);
    }
    
    public Item[] getOrderList() {
        Item[] tmp = new Item[orderList.size()];
        orderList.toArray(tmp);
        return tmp;
    }

    public int getCount() {
        return count;
    }
    
    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    public boolean addObject(Item inBarang){
        if(orderList.add(inBarang)){
            count = orderList.size();
            return true;
        }
        else{
            return false;
        }
    }
    
    public Item findObject(Item inBarang){
        Item tmp;
        for (Object orderListItem : orderList) {
            tmp = (Item) orderListItem;
            if(tmp.getId() == inBarang.getId()){
                return tmp;
            }
        }
        return null;
    }
    
    public boolean remObject(Item inBarang){
        if(orderList.remove(inBarang)){
            count = orderList.size();
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean clearOrder(){
        if(orderList.removeAll(orderList)){
            this.count = orderList.size();
            this.totalPrice  = 0;
            this.totalProfit = 0;
            return true;
        }
        else{
            return false;
        }
    }
    
    public static int getProfit_margin(){
        return profit_margin;
    }

    public static void setProfit_margin(int aProfit_margin){
        profit_margin = aProfit_margin;
    }
    
    public void commit(){
        //private ArrayList<Item> orderList;
        int profit;
        int totalProfit = 0;
        int totalPrice = 0;
        
        for (Item orderList1 : orderList) {
            //profit margin in integer lol... need to divide by hundreds
            profit = (orderList1.getPrice() *profit_margin) / 100;
            totalProfit += profit * orderList1.getAmount();
            totalPrice += (orderList1.getPrice() + (profit)) * orderList1.getAmount();
        }
        
        this.totalProfit = totalProfit;
        this.totalPrice = totalPrice;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public int getTotalProfit() {
        return totalProfit;
    }
    
    /**
     * Find object by barcode
     * @param barcode
     * @return
     */
    public Item findObjectbyBarcode(String barcode){
        Item out;
        for (Object orderListItem : orderList) {
            out = (Item) orderListItem;
            if(out.getBarcode().equals(barcode)){
                return out;
            }
        }
        return null;
    }
}
