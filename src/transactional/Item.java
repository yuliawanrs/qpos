/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactional;

/**
 * Store exact one type of sale item
 * @author Tezla
 */
public class Item {
    private final int id;
    private String name;
    private String barcode;
    private int price;
    private int amount;
    
    private final int priceProfit;
    private int totalPrice;
    
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }
    
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    public void setAmount(int amount) {
        this.amount = amount;
    }
       
    public Item(int id, String name, int amount, int price, String barcode){
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.barcode = barcode;
        this.priceProfit = price + ((price * Order.getProfit_margin())/100);
    }

    public String getBarcode() {
        return barcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }    

    public int getTotalPrice() {
        this.totalPrice = this.amount * this.getPriceProfit();
        return totalPrice;
    }

    public int getPriceProfit() {
        return priceProfit;
    }
}
