/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queries;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import transactional.*;
import user.*;
/**
 * NEED JDBC Driver, coding by force, may force with me :v
 * @author Tezla
 * @coauthor yuliawanrs
 */
public class ObjectFactory {
    /* 
    *  db configuration goes here
    */
    private final String server   = "localhost";
    private final String database = "project_oose";
    private final String username = "root";
    private final String password = "";
    
    /*
    * Should be initialized upon construction
    */
    private static Connection conn = null;
    
    /*
    * Singleton
    */
    
    private static ObjectFactory myInstance;
    
    /**
     * Initiates ObjectFactory building connection, if it fails, just destroy the object
     */
    private ObjectFactory(){
        try
                {
                    //initiate connection
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    ObjectFactory.conn = DriverManager.getConnection("jdbc:mysql://"+ this.server +"/" + this.database + "?user=" + this.username + "&password=" + this.password);
                    
                    //get configuration (profit margin)
                    Statement stmt = conn.createStatement();
                    ResultSet trest = stmt.executeQuery("SELECT * FROM configs");
                    while(trest.next()){
                        if(trest.getString("param_name").equals("profit_margin")){
                            Order.setProfit_margin(trest.getInt("value"));
                        }
                    }
                    
                }
        catch(SQLException excepts)
                {
                    System.out.println("SQLException: " + excepts.getMessage());
                    System.out.println("SQLState: " + excepts.getSQLState());
                    System.out.println("VendorError: " + excepts.getErrorCode());
                }
        catch(ClassNotFoundException | InstantiationException | IllegalAccessException excepts)
                {
                    System.out.println("JavaException: " + excepts.getMessage());
                }
    }
    
    /**
     * The only gateway to use ObjectFactory
     * @return ObjectFactory instance
     */
    public static ObjectFactory getInstance(){
        if(myInstance == null || conn == null){
            myInstance = new ObjectFactory();
            //configuration hooks
            myInstance.getSettings();
        }
        return myInstance;
    }
    
    /**
     * Get users on given type, returns null if no query result existed
     * @param userType which is statically defined on database (refer to user_type table on project_oose.sql
     * @return
     */
    public User[] getAllUserType(String userType){
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet trest = stmt.executeQuery("SELECT id FROM user_types WHERE type like '" + userType + "'");
            int qid = 0;
            if(trest.next()){
                qid = trest.getInt("id");
            }
            ResultSet rest = stmt.executeQuery("SELECT * FROM users WHERE type_id = " + qid);
            //get number of rows
            rest.last();
            if(rest.getRow() > 0){
                //global type storage - user
                User storage[] = new User[rest.getRow()];
                rest.beforeFirst();
                //generate objects to be returned
                int index = 0;
                while(rest.next()){
                    //quick patch - not a good solution
                    if(userType.equalsIgnoreCase("Cashier")){
                        //Cashier(int id, String name, String password)
                        Cashier tmp = new Cashier(rest.getInt("id"), rest.getString("user_name"), rest.getString("password"));
                        storage[index] = tmp;
                        index++;
                    }
                    else if(userType.equalsIgnoreCase("Manager")){
                        //Cashier(int id, String name, String password)
                        Manager tmp = new Manager(rest.getInt("id"), rest.getString("user_name"), rest.getString("password"));
                        storage[index] = tmp;
                        index++;
                    }
                    else if(userType.equalsIgnoreCase("Supplier")){
                        //Cashier(int id, String name, String password)
                        Supplier tmp = new Supplier(rest.getInt("id"), rest.getString("user_name"), rest.getString("password"));
                        storage[index] = tmp;
                        index++;
                    }
                }
                return storage;
            }
            return null;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return null;
    }
    
    public User getUserWithName(String UserName){
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT * FROM users WHERE user_name like '" + UserName + "'");
            User storage = null;
            rest.beforeFirst();
            if(rest.next()){
                int userType = rest.getInt("type_id");
                //hard coded, not a good approach
                if(userType == 1){
                    //supplier
                    storage = new Supplier(rest.getInt("id"), rest.getString("user_name"), rest.getString("password"));
                }
                else if(userType == 2){
                    //manager
                    storage = new Manager(rest.getInt("id"), rest.getString("user_name"), rest.getString("password"));
                }
                else if(userType == 3){
                    //cashier
                    storage = new Cashier(rest.getInt("id"), rest.getString("user_name"), rest.getString("password"));
                }
            }
            return storage;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return null;
    }
    
    /**
     * Get all item within items table, returns null if no query result existed
     * @return
     */
    public Item[] getAllItem(){
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT * FROM items");
            //get number of rows
            rest.last();
            if(rest.getRow() > 0){
                Item storage[] = new Item[rest.getRow()];
                rest.beforeFirst();
                //generate objects to be returned
                int index = 0;
                while(rest.next()){
                    //Item(int id, String name, int amount, int price, String barcode)
                    Item tmp = new Item(rest.getInt("id"), rest.getString("name"), rest.getInt("amount"), rest.getInt("price"), rest.getString("barcode"));
                    storage[index] = tmp;
                    index++;
                }
                return storage;
            }
            return null;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return null;
    }
    
        /**
     * Get all item within items table, returns null if no query result existed
     * @param barcode
     * @return
     */
    public Item[] findItem(String column, String criteria){
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT * FROM items WHERE " + column + " like '%" + criteria + "%'");
            //get number of rows
            rest.last();
            if(rest.getRow() > 0){
                Item storage[] = new Item[rest.getRow()];
                rest.beforeFirst();
                //generate objects to be returned
                int index = 0;
                while(rest.next()){
                    //Item(int id, String name, int amount, int price, String barcode)
                    Item tmp = new Item(rest.getInt("id"), rest.getString("name"), rest.getInt("amount"), rest.getInt("price"), rest.getString("barcode"));
                    storage[index] = tmp;
                    index++;
                }
                return storage;
            }
            return null;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return null;
    }
    
    
    /**
     * Insert new Item object into database
     * @param objs
     * @return
     */
    public boolean saveNewItem(Item objs){
        try
        {
            Statement stmt = conn.createStatement();
            //id is "optionalised" so, it will use auto_increment
            Item isExist = this.findSingleExactItem("barcode", objs.getBarcode());
            if(isExist == null){
                stmt.executeUpdate("INSERT INTO items(barcode, name, price, amount) VALUES ("+ "'" + objs.getBarcode() + "','" + objs.getName() + "'," + objs.getPrice() + "," + objs.getAmount() + ")");
                return true;
            }
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return false;
    }
    
    /**
     * Should invoke this chant if  you wish to load Profit Margin - well, camel's dang, i can't hook these code into the constructor..
     * @return boolean
     */
    public boolean getSettings(){
        try {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT * FROM configs WHERE param_name = 'profit_margin'");
            boolean out = false;
            rest.last();
            if(rest.getRow() > 0){
                rest.beforeFirst();
                //assume first item
                if(rest.next()){
                    Order.setProfit_margin(rest.getInt("value"));
                    out = true;
                }
            }
            return out;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return false;
    }
    
    /**
     * Should invoke this chant if  you wish to load Profit Margin - well, camel's dang, i can't hook these code into the finalize().. destructor
     * @return boolean
     */
    public boolean saveSettings(){
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("UPDATE configs SET value = " + Order.getProfit_margin() + " WHERE param_name = 'profit_margin'");
            return true;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return false;
    }
    
    private int getOrderNextId(){
        int id = 0;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SHOW TABLE STATUS LIKE 'orders' ");
            rest.last();
            if(rest.getRow() > 0){
                rest.beforeFirst();
                //assume first item
                if(rest.next()){
                    id = rest.getInt("Auto_Increment");
                }
            }
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return id;
    }
    
    /**
     * Saves Order object to database
     * @param inOrder
     * @param inUser
     * @return
     */
    public boolean saveOrder(Order inOrder, User inUser){
        try {
            Statement stmt = conn.createStatement();
            //counts totalPrice and totalProfit
            inOrder.commit();
            stmt.executeUpdate("INSERT INTO orders (user_id, count, total_profit, total_price) values (" + inUser.getId() + "," + inOrder.getCount() + "," + inOrder.getTotalProfit() + "," + inOrder.getTotalPrice() + ")");
            //concurrency not save
            int id = this.getOrderNextId() - 1;
            if(inOrder.getOrderList() != null){
                for(Item inItem : inOrder.getOrderList()){
                    stmt.executeUpdate("UPDATE items SET amount = amount - " + inItem.getAmount() + " WHERE id = " + inItem.getId());
                    stmt.executeUpdate("INSERT INTO order_lists (order_id, item_id, amount) values (" + id + "," + inItem.getId() + "," + inItem.getAmount() + ")");
                }
            }
            return true;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return false;
    }
    
    public int getTotalSales(){
        int val = 0;
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT count(id) as counted FROM orders");
            rest.beforeFirst();
            if(rest.next()){
                val = rest.getInt("counted");
            }
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return val;
    }
    
    public int getCashFlow(){
        int val = 0;
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT sum(total_price) as counted FROM orders");
            rest.beforeFirst();
            if(rest.next()){
                val = rest.getInt("counted");
            }
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return val;
    }
    
    public int getProfit(){
        int val = 0;
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT sum(total_profit) as counted FROM orders");
            rest.beforeFirst();
            if(rest.next()){
                val = rest.getInt("counted");
            }
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return val;
    }
    
    public boolean saveItem(Item objs){
        try
        {
            Statement stmt = conn.createStatement();
            //id is "optionalised" so, it will use auto_increment
            stmt.executeUpdate("UPDATE items SET barcode = '" + objs.getBarcode() +  "' , name = '" + objs.getName() + "', price = " + objs.getPrice() + ", amount = " + objs.getAmount() + " WHERE id = " + objs.getId() + ";");
            return true;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return false;
    }
    
    public Item findSingleExactItem(String column, String criteria){
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rest = stmt.executeQuery("SELECT * FROM items WHERE " + column + " = '" + criteria + "'");
            //get number of rows
            rest.last();
            if(rest.getRow() > 0){
                Item storage = null;
                rest.beforeFirst();
                //assume only to get first item;
                if(rest.next()){
                    //Item(int id, String name, int amount, int price, String barcode)
                    Item tmp = new Item(rest.getInt("id"), rest.getString("name"), rest.getInt("amount"), rest.getInt("price"), rest.getString("barcode"));
                    storage = tmp;
                }
                return storage;
            }
            return null;
        }
        catch(SQLException excepts)
        {
            System.out.println("SQLException: " + excepts.getMessage());
            System.out.println("SQLState: " + excepts.getSQLState());
            System.out.println("VendorError: " + excepts.getErrorCode());
        }
        return null;
    }
    
    @Override
    protected void finalize(){
        try {
            //saves Order's profit margin upon destruction.. - not working as intended, it just puff, destroyed before saving!
            this.saveSettings();
            //closes the connection
            conn.close();
            super.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(ObjectFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
