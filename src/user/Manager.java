/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import transactional.Order;

/**
 *
 * @author Yuliawan
 */
public class Manager extends User {

    public Manager(int id, String name, String password) {
        super(id, name, password);
    }
    
    public void setProfitMargin(int in){
        Order.setProfit_margin(in);
    }
    
}
