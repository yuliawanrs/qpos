/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;
import transactional.Order;

/**
 *
 * @author Yuliawan
 */
public class Cashier extends User {
    private Order currentOrder;
    
    public Cashier(int id, String name, String password) {
        super(id, name, password);
    }
    
    public void initiateOrder(){
        //initiate order and forgets any previous order
        this.currentOrder = new Order();
    }
    
    public void cancelOrder(){
        // huh?
    }
    
    public boolean commitOrder(){
        //commit order to sql, return true if query success
        return true;
    }
    
    
}
